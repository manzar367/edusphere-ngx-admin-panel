import { Component, OnInit, ElementRef  } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
//import  *  as  data  from  '../../usersData.json';
import { HttpClient , HttpParams , HttpHeaders} from '@angular/common/http';
import { FormBuilder, FormGroup } from "@angular/forms";


@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit{

  form: FormGroup;
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      _id: {
        title: 'ID',
        type: 'string',
      },
      firstname: {
        title: 'First Name',
        type: 'string',
      },
      lastname: {
        title: 'Last Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      phone: {
        title: 'Phone Number',
        type: 'number',
      },
      mobileVerification: {
        title: 'Mobile Verification',
        type: 'boolean',
      },
      emailVerification: {
        title: 'Email Verification',
        type: 'boolean',
      },
      password: {
        title: 'Password',
        type: 'string',
      },
      userType: {
        title: 'User Type',
        type: 'string',
      },
      dialCode: {
        title: 'Dial Code',
        type: 'number',
      },
      countryCode: {
        title: 'Country Code',
        type: 'number',
      },
      mediumOfRegistration: {
        title: 'Medium Of Registration',
        type: 'string',
      },
      noOfChildrens: {
        title: 'No Of Childrens',
        type: 'number',
      },
      address:{
        title: 'Address',
        type: 'string',
      },
      enabled: {
        title: 'Enabled',
        type: 'boolean',
      },
      otp: {
        title: 'OTP',
        type: 'number',
      },
      createdOn: {
        title: 'Created On ',
        type: 'date',
      },
      updatedOn: {
        title: 'Updated On ',
        type: 'date',
      },
      __v: {
        title: 'Version ',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
    },
  };
  
  source: LocalDataSource = new LocalDataSource();

   sendGETRequestWithParameters(){
    
    let headers = new HttpHeaders().set('userType', 'ADMIN');
    let params = new HttpParams();
   
    params = params.append('type', 'PARENT');
    params = params.append('skip', '0');
    params = params.append('limit', '10');

    return this.httpClient.get("http://159.89.193.213:8081/api/admin/users", {headers: headers, params: params}).subscribe((res)=>{
          console.log(res.data);
          this.source.load(res.data);
     });
   
   }
   
  // source: LocalDataSource = new LocalDataSource();
   // products:  any  = (data  as  any).default;
  
  constructor(private service: SmartTableData ,public httpClient: HttpClient , private el: ElementRef) {
     
    //this.source.load(this.products.data);
    //  this.source.load(this.userData);
    
  }
   
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  

  closeFunction(){
    let myTag = this.el.nativeElement.querySelector(".settings-sidebar");
    myTag.classList.add('collapsed'); 
    myTag.classList.remove('expanded'); 
    myTag.classList.remove('dataSideBar');
   // console.log("Hide");
  }

  
  isReadonly = true;
  _id:string;
  firstname: string;  
  lastname: string; 
  email:string;
  phone:string;
  address:string;
  // password:string;
  countryCode:string;
  mediumOfRegistration:string;
  noOfChildrens:number;
  status:string;
  mobileVerification:boolean;
  emailVerification:boolean;
  
  onModalShow(event):void{
    let rowAddClass = this.el.nativeElement.querySelector('.settings-sidebar');
    rowAddClass.classList.add('expanded');
    rowAddClass.classList.add('dataSideBar');
    rowAddClass.classList.remove('collapsed'); 
    // console.log("Show");
    // console.log("Selected Data",event.data); 
    // console.log("ID",event.data._id); 

    let headers = new HttpHeaders().set('userType', 'ADMIN');
    let params = new HttpParams();
   
    params = params.append('type', 'PARENT');
    params = params.append('skip', '0');
    params = params.append('limit', '10');

     this.httpClient.get("http://159.89.193.213:8081/api/admin/users", {headers: headers, params: params}).subscribe((res)=>{
      
          for(var i = 0; i < res.data.length; i++){
            if(event.data._id === res.data[i]._id){
             // console.log("Selected API Data",res.data[i]);
               //this.firstname =  res.data[i].firstname;
              // this.lastname =  res.data[i].lastname;
               this.email =  res.data[i].email;
               this.phone =  res.data[i].phone;
               this.countryCode =  res.data[i].countryCode;
               this.mediumOfRegistration =  res.data[i].mediumOfRegistration;
               this.noOfChildrens =  res.data[i].noOfChildrens;
               this.status =  res.data[i].status;
             //  this.address = res.data[i].address;
               this._id = res.data[i]._id;
               this.emailVerification = res.data[i].emailVerification;
               this.mobileVerification = res.data[i].mobileVerification;
            }
          }
     });
  }
 
  editFunction(){
     this.isReadonly = !this.isReadonly;
  }

  $formData = [];
  newDataObj = {};
  updateFirstName:string;
  updateLastName:string;
  updateEmail:string;
  updatePhone:string;
  updateCountry:string;
  updateAddress:string;
  updateUsermedium:string;
  updateChildrens:number;
  updateStatus:string;

  private focusoutHandlerFirstName(event) {
    this.updateFirstName = event.target.value;
  }
  private focusoutHandlerLastName(event) {
    this.updateLastName = event.target.value;
  }
  private focusoutHandlerEmail(event) {
    this.updateEmail = event.target.value;
  }
  private focusoutHandlerPhone(event) {
    this.updatePhone = event.target.value;
  }
  private focusoutHandlerCountry(event) {
    this.updateCountry = event.target.value;
  }
  private focusoutHandlerUsermedium(event) {
    this.updateUsermedium = event.target.value;
  }
  private focusoutHandlerChildrens(event) {
    this.updateChildrens = event.target.value;
  }
  private focusoutHandlerAddress(event){
    this.updateAddress = event.target.value;
  }
  private focusoutHandlerStatus(event) {
    this.updateStatus = event.target.value;
  }

  // saveEditData(event):void{
  //   this.focusoutHandlerName;
  //   this.focusoutHandlerEmail;
  //   this.focusoutHandlerPhone;
  //   this.focusoutHandlerCountry;
  //   this.focusoutHandlerUsermedium;
  //   this.focusoutHandlerChildrens;
  //   this.focusoutHandlerStatus;
  //   this.isReadonly = !this.isReadonly;
  //   var updateArrData = [
  //     { name: this.updateName, email: this.updateEmail , phone: this.updatePhone , countryCode:  this.updateCountry, mediumOfRegistration: this.updateUsermedium , noOfChildrens: this.updateChildrens , status: this.updateStatus}
  //   ];
  //   console.log(updateArrData);
  //   console.log(this._id);
  //   //if(this.updateName === undefined || this.updateEmail === undefined)
  // }

  saveEditData() {
    this.focusoutHandlerFirstName;
    this.focusoutHandlerLastName
    this.focusoutHandlerEmail;
    this.focusoutHandlerPhone;
    this.focusoutHandlerCountry;
    this.focusoutHandlerUsermedium;
    this.focusoutHandlerChildrens;
    this.focusoutHandlerStatus;
    this.focusoutHandlerAddress;

    let headers = new HttpHeaders().set('usertype', 'ADMIN');

    this.isReadonly = !this.isReadonly;
    var formData: any = new FormData();

    formData.append("sourceOfRegistration", "ADMIN");

    if(this.updateFirstName !== undefined){
      formData.append("firstname", this.updateFirstName);
    } else{
      formData.append("firstname", this.firstname);
    }
    if(this.updateLastName !== undefined){
      formData.append("lastname", this.updateLastName);
    } else{
      formData.append("lastname", this.lastname);
    }

    if(this.updateEmail !== undefined){
      formData.append("email",this.updateEmail);
    } else{
      formData.append("email", this.email);
    }

    if(this.updatePhone !== undefined){
      formData.append("phone",this.updatePhone);
    } else{
      formData.append("phone", this.phone);
    }
    
    if(this.updateCountry !== undefined){
      formData.append("countryCode",this.updateCountry);
    } else{
      formData.append("countryCode", this.countryCode);
    }
    
    if(this.updateUsermedium !== undefined){
      formData.append("mediumOfRegistration", this.updateUsermedium);
    } else{
      formData.append("mediumOfRegistration", this.mediumOfRegistration);
    }
    
    if(this.updateChildrens !== undefined){
      formData.append("noOfChildrens",this.updateChildrens);
    } else{
      formData.append("noOfChildrens", this.noOfChildrens);
    }

    if(this.updateAddress !== undefined){
      formData.append("address",this.updateAddress);
    } else{
      formData.append("address", this.address);
    }

    if(this.updateStatus !== undefined){
      formData.append("status", this.updateStatus);
    } else{
      formData.append("status", this.status);
    }
    
    
    
    console.log("data:",formData);
    this.httpClient.post('http://localhost:8081/api/admin/user', formData , {headers: headers}).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )
  }

  ngOnInit(){
    this.sendGETRequestWithParameters();
  }
 

}
