import { NgModule,Directive,Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbTreeGridModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsersComponent } from './users.component';
import { ThemeModule } from '../../@theme/theme.module';
import { BrowserModule }    from '@angular/platform-browser';  
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Directive({
  selector: '[readonly],[readOnly]',
  host: {
    '[attr.readonly]': '_isReadonly ? "" : null'
  }
})
class ReadonlyDirective {
  _isReadonly = false;

  @Input() set readonly (v) {
     this._isReadonly = coerceBooleanProperty(v);
  };

  ngOnChanges(changes) {
    console.log(changes);
  }
}

@NgModule({
  declarations: [UsersComponent,ReadonlyDirective],
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    BrowserModule,  
    FormsModule,
    ReactiveFormsModule,
// import HttpClientModule after BrowserModule.  
    HttpClientModule, 
  ],
  providers:[]
})
export class UsersModule { }
