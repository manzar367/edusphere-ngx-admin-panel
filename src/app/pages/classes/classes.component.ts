import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
@Component({
  selector: 'ngx-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss']
})
export class ClassesComponent {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Id',
        type: 'string',
      },
      subject: {
        title: 'Subject Name',
        type: 'string',
      },
      subjectId: {
        title: 'Subject Id',
        type: 'string',
      },
      location: {
        title: 'Location',
        type: 'string',
      },
      date: {
        title: 'Date',
        type: 'string',
      },
      stdId: {
        title: 'Student Id',
        type: 'string',
      },
      coachId: {
        title: 'Coach Id',
        type: 'string',
      },
      addressId: {
        title: 'Address Id',
        type: 'string',
      },
      duration: {
        title: 'Duration(in min)',
        type: 'number',
      },
      classType: {
        title: 'Plan Type',
        type: 'string',
      },
      maxStd: {
        title: 'Max Students',
        type: 'number',
      },
      nom: {
        title: 'Number Of Month',
        type: 'number',
      },
      createOn: {
        title: 'Created On ',
        type: 'string',
      },
      updateOn: {
        title: 'Updated On ',
        type: 'string',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData) {
  //  const data = this.service.getData();
    const data = [
      { id:'class85658sd', subject: 'Cricket', subjectId:'cricket11@out', location: 'Malkpet', date: '17 June 2020', stdId:'manzar@3677', coachId:'hussain@1995', addressId:'malkpet@207', duration:60, classType:'weakly', maxStd:11, nom: 3, createOn:'16 June 2020', updateOn:'20 June 2020', status:'Enable'},
      { id:'class968658g', subject: 'Football', subjectId:'football11@goal', location: 'Ameerpet', date: '18 June 2020', stdId:'aerik@9686', coachId:'hanna@2256', addressId:'ameerpet@292', duration:30, classType:'Daily', maxStd:22, nom: 5, createOn:'15 June 2020', updateOn:'21 June 2020', status:'Enable'},
      { id:'class321685p', subject: 'Hockey', subjectId:'hockey11@hit', location: 'Jubli Hills', date: '19 June 2020', stdId:'peter@2154', coachId:'paker@5687', addressId:'jublihills@9999', duration:45, classType:'weakly', maxStd:11, nom: 1, createOn:'18 May 2020', updateOn:'31 May 2020', status:'Enable'},
      { id:'class123654e', subject: 'Badminton', subjectId:'badminton4@hit', location: 'Banjara Hills', date: '20 June 2020', stdId:'Katpari@96568', coachId:'perrilie@1245', addressId:'banjarahills@8975', duration:30, classType:'Daily', maxStd:6, nom: 4, createOn:'15 May 2020', updateOn:'28 May 2020', status:'Enable'},
      { id:'class36987es', subject: 'Carrom board', subjectId:'carrom04@goal', location: 'Yosufnagar', date: '21 June 2020', stdId:'harrypoter@87565', coachId:'poter@85654', addressId:'yosufnagar@89656', duration:15, classType:'weakly', maxStd:4, nom: 7, createOn:'10 May 2020', updateOn:'29 May 2020', status:'Desable'},
      { id:'class93286ss', subject: 'Chess Board', subjectId:'chess02@win', location: 'Dilsukhnagar', date: '22 June 2020', stdId:'nancy@1697', coachId:'happy@15556', addressId:'dilsukhnagar@6548', duration:30, classType:'Daily', maxStd:2, nom: 8, createOn:'10 April 2020', updateOn:'15 May 2020', status:'Enable'},
      { id:'class558654l', subject: 'Long Tennis', subjectId:'longtennis04@win', location: 'Hi-Tech City', date: '23 June 2020', stdId:'humber@5685', coachId:'burgger@99556', addressId:'hitech@4758', duration:60, classType:'weakly', maxStd:4, nom: 9, createOn:'5 April 2020', updateOn:'26 April 2020', status:'Enable'},
      { id:'class785462r', subject: 'Table Tennis', subjectId:'tabletennis02@win', location: 'Madhapur', date: '24 June 2020', stdId:'simran@89658', coachId:'raj@968755', addressId:'madhapur@2548', duration:45, classType:'Daily', maxStd:8, nom: 12, createOn:'2 March 2020', updateOn:'27 April 2020', status:'Enable'},
      { id:'class93125sa', subject: 'Swimming', subjectId:'swimming07@deep', location: 'Panjagutta ', date: '25 June 2020', stdId:'irshad@8695', coachId:'ali@85695', addressId:'panjagutta@2807', duration:30, classType:'weakly', maxStd:7, nom: 2, createOn:'1 March 2020', updateOn:'20 March 2020', status:'Enable'},
      { id:'class24563lo', subject: 'Math Classes', subjectId:'mathclass20@learn', location: 'Himayat Nagar', date: '26 June 2020', stdId:'jawed@78569', coachId:'khan@58569', addressId:'himayatnagar@8577', duration:60, classType:'Daily', maxStd:20, nom: 6, createOn:'9 Feb 2020', updateOn:'12 March 2020', status:'Enable'},
      { id:'class963215d', subject: 'Dance', subjectId:'dance09@move', location: 'Golconda', date: '27 June 2020', stdId:'ketty@45875', coachId:'liee@474586', addressId:'golconda@3547', duration:45, classType:'weakly', maxStd:5, nom: 10, createOn:'16 June 2020', updateOn:'17 July 2020', status:'Enable'},
      { id:'class124875k', subject: 'Singing', subjectId:'singing05@voice', location: 'Mehdipatnam', date: '28 June 2020', stdId:'pooljack@85695', coachId:'humburg@89758', addressId:'mehdipatnam@12407', duration:30, classType:'Daily', maxStd:9, nom: 11, createOn:'4 Feb 2020', updateOn:'13 June 2020', status:'Enable'},
      { id:'class86598jj', subject: 'English Spoken', subjectId:'english15@speek', location: 'Koti', date: '29 June 2020', stdId:'sambur@85689', coachId:'catni@897586', addressId:'koti@856487', duration:60, classType:'weakly', maxStd:12, nom: 12, createOn:'3 Feb 2020', updateOn:'6 June 2020', status:'Enable'},
      { id:'class321486w', subject: 'French Spoken', subjectId:'french17@speek', location: 'Abids', date: '30 June 2020', stdId:'ashaan@9008', coachId:'khan@09659', addressId:'abids@25407', duration:60, classType:'Daily', maxStd:10, nom: 1, createOn:'19 Jan 2020', updateOn:'20 May 2020', status:'Enable'},
      { id:'class898568b', subject: 'Piano Classes', subjectId:'piano01@tune', location: 'Lb Nagar', date: '1 Aug 2020', stdId:'srk@1995', coachId:'mannat@78658', addressId:'lbnagar@965856', duration:30, classType:'weakly', maxStd:1, nom: 2, createOn:'29 Jan 2020', updateOn:'21 May 2020', status:'Enable'},
      
  ];
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
