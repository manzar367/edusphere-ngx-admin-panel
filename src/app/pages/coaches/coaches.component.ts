import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
import { HttpClient , HttpParams , HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'ngx-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.scss']
})
export class CoachesComponent implements OnInit {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      _id: {
        title: 'ID',
        type: 'string',
      },
      name: {
        title: 'Full Name',
        type: 'string',
      },
    
      email: {
        title: 'E-mail',
        type: 'string',
      },
      phone: {
        title: 'Phone Number',
        type: 'number',
      },
      mobileVerification: {
        title: 'Mobile Verification',
        type: 'boolean',
      },
      emailVerification: {
        title: 'Email Verification',
        type: 'boolean',
      },
      password: {
        title: 'Password',
        type: 'string',
      },
      userType: {
        title: 'User Type',
        type: 'string',
      },
      dialCode: {
        title: 'Dial Code',
        type: 'number',
      },
      countryCode: {
        title: 'Country Code',
        type: 'number',
      },
      mediumOfRegistration: {
        title: 'Medium Of Registration',
        type: 'string',
      },
      noOfChildrens: {
        title: 'No Of Childrens',
        type: 'number',
      },
      enabled: {
        title: 'Enabled',
        type: 'boolean',
      },
      otp: {
        title: 'OTP',
        type: 'number',
      },
      createdOn: {
        title: 'Created On ',
        type: 'date',
      },
      updatedOn: {
        title: 'Updated On ',
        type: 'date',
      },
      __v: {
        title: 'Version ',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
    },
  };

  public userData : any;
  source: LocalDataSource = new LocalDataSource();
   sendGETRequestWithParameters(){
    
    let headers = new HttpHeaders().set('userType', 'ADMIN');
    let params = new HttpParams();
   
    params = params.append('type', 'COACH');
    params = params.append('skip', '0');
    params = params.append('limit', '10');

    return this.httpClient.get("http://159.89.193.213:8081/api/admin/users", {headers: headers, params: params}).subscribe((res)=>{
          console.log(res.data);
          this.source.load(res.data);
     });
   
   }

  // source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,public httpClient: HttpClient) {
  
   // this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  ngOnInit(){

    this.sendGETRequestWithParameters();
    
   }
}
