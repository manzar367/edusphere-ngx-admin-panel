import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CoachesComponent } from './coaches.component';
import { ThemeModule } from '../../@theme/theme.module';
import { BrowserModule }    from '@angular/platform-browser';  
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [CoachesComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    BrowserModule,  
// import HttpClientModule after BrowserModule.  
    HttpClientModule, 
  ]
})
export class CoachesModule { }
