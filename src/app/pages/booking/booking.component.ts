import { Component,OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
import { HttpClient , HttpParams , HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'ngx-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit{
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      _id:{
        title: 'Id',
        type: 'string',
      },
      subjectName: {
        title: 'Subject Name',
        type: 'string',
      },
      studentName: {
        title: 'Student Name',
        type: 'string',
      },
      classCreateDate: {
        title: 'Class CreatedOn',
        type: 'string',
      },
      coachName: {
        title: 'Coach Name',
        type: 'string',
      },
      parentName: {
        title: 'Parent Name',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      slotStart: {
        title: 'Slot Start',
        type: 'string',
      },
      slotEnd:{
        title: 'Slot End',
        type: 'string',
      },
      fees: {
        title: 'Fees',
        type: 'string',
      },
      enabled:{
        title: 'Enabled ',
        type: 'boolean',
      },
      createdOn: {
        title: 'Created On ',
        type: 'date',
      },
      updatedOn: {
        title: 'Updated On ',
        type: 'date',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
    },
  };
  public userData : any;
  public subjectName : {};
  source: LocalDataSource = new LocalDataSource();
   sendGETRequestWithParameters(){
    
    let headers = new HttpHeaders().set('userType', 'ADMIN');
    let params = new HttpParams();
   
    //params = params.append('type', 'BOOKING');
    params = params.append('skip', '0');
    params = params.append('limit', '15');

    return this.httpClient.get("http://localhost:8081/api/admin/bookings", {headers: headers, params: params}).subscribe((res)=>{
          for(var i = 0; i < res.data.length; i++){
            res.data[i].subjectName = res.data[i].subjectId.name;
            res.data[i].studentName = res.data[i].studentId.name;
            res.data[i].parentName = res.data[i].parentId.name;
            res.data[i].coachName = res.data[i].coachId.name;
            res.data[i].fees = res.data[i].slotId.fees;
            res.data[i].slotStart = res.data[i].slotId.slotStart;
            res.data[i].slotEnd = res.data[i].slotId.slotEnd;
            res.data[i].classCreateDate = res.data[i].classId.createdOn;
            res.data[i].address = res.data[i].planId.addressId.addressName;
          }
         
          console.log(res.data);
          this.source.load(res.data);
     });
   
   }
  //source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,public httpClient: HttpClient) {
  
    //this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  ngOnInit(){

    this.sendGETRequestWithParameters();
    
   }
   
}
