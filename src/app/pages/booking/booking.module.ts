import { NgModule , APP_INITIALIZER} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BookingComponent } from './booking.component';
import { ThemeModule } from '../../@theme/theme.module';
import { BrowserModule }    from '@angular/platform-browser';  
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [BookingComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    BrowserModule,
    HttpClientModule,
  ],
  providers: [
    { provide: APP_INITIALIZER, useValue: '',  multi: true }
  ]
})
export class BookingModule { }
